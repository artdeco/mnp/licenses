# Licenses

This is a list of Licenses that can be used with MNP.

<a name="table-of-contents"></a>

- [Academic Free License v3.0](#academic-free-license-v30)
- [Apache license 2.0](#apache-license-20)
- [Artistic license 2.0](#artistic-license-20)
- [Boost Software License 1.0](#boost-software-license-10)
- [BSD 2-clause "Simplified" license](#bsd-2-clause-simplified-license)
- [BSD 3-clause "New" or "Revised" license](#bsd-3-clause-new-or-revised-license)
- [BSD 3-clause Clear license](#bsd-3-clause-clear-license)
- [Creative Commons Zero v1.0 Universal](#creative-commons-zero-v10-universal)
- [Creative Commons Attribution 4.0](#creative-commons-attribution-40)
- [Creative Commons Attribution Share Alike 4.0](#creative-commons-attribution-share-alike-40)
- [Do What The F*ck You Want To Public License](#do-what-the-fck-you-want-to-public-license)
- [Educational Community License v2.0](#educational-community-license-v20)
- [Eclipse Public License 1.0](#eclipse-public-license-10)
- [European Union Public License 1.1](#european-union-public-license-11)
- [GNU Affero General Public License v3.0](#gnu-affero-general-public-license-v30)
  * [In-Code Assertion](#in-code-assertion)
- [GNU General Public License v2.0](#gnu-general-public-license-v20)
- [GNU General Public License v3.0](#gnu-general-public-license-v30)
  * [In-Code Assertion](#in-code-assertion)
- [GNU Lesser General Public License v2.1](#gnu-lesser-general-public-license-v21)
- [GNU Lesser General Public License v3.0](#gnu-lesser-general-public-license-v30)
  * [In-Code Assertion](#in-code-assertion)
- [ISC](#isc)
- [LaTeX Project Public License v1.3c](#latex-project-public-license-v13c)
- [Microsoft Public License](#microsoft-public-license)
- [MIT](#mit)
  * [In-Code Assertion](#in-code-assertion)
- [Mozilla Public License 2.0](#mozilla-public-license-20)
- [Open Software License 3.0](#open-software-license-30)
- [PostgreSQL License](#postgresql-license)
- [SIL Open Font License 1.1](#sil-open-font-license-11)
- [University of Illinois/NCSA Open Source License](#university-of-illinoisncsa-open-source-license)
- [The Unlicense](#the-unlicense)
- [zLib License](#zlib-license)

<div align="center"><a href="#table-of-contents">
  <img src="/.documentary/section-breaks/0.svg?sanitize=true">
</a></div>

<details>
  <summary>Show Summary</summary>

  ```json
  {
    "afl-3.0": {
      "name": "Academic Free License v3.0",
      "spdx": "AFL-3.0"
    },
    "apache-2.0": {
      "name": "Apache license 2.0",
      "spdx": "Apache-2.0"
    },
    "artistic-2.0": {
      "name": "Artistic license 2.0",
      "spdx": "Artistic-2.0"
    },
    "bsl-1.0": {
      "name": "Boost Software License 1.0",
      "spdx": "BSL-1.0"
    },
    "bsd-2-clause": {
      "name": "BSD 2-clause \"Simplified\" license",
      "spdx": "BSD-2-Clause"
    },
    "bsd-3-clause": {
      "name": "BSD 3-clause \"New\" or \"Revised\" license",
      "spdx": "BSD-3-Clause"
    },
    "bsd-3-clause-clear": {
      "name": "BSD 3-clause Clear license",
      "spdx": "BSD-3-Clause-Clear"
    },
    "cc0-1.0": {
      "name": "Creative Commons Zero v1.0 Universal",
      "spdx": "CC0-1.0"
    },
    "cc-by-4.0": {
      "name": "Creative Commons Attribution 4.0",
      "spdx": "CC-BY-4.0"
    },
    "cc-by-sa-4.0": {
      "name": "Creative Commons Attribution Share Alike 4.0",
      "spdx": "CC-BY-SA-4.0"
    },
    "wtfpl": {
      "name": "Do What The F*ck You Want To Public License",
      "spdx": "WTFPL"
    },
    "ecl-2.0": {
      "name": "Educational Community License v2.0",
      "spdx": "ECL-2.0"
    },
    "epl-1.0": {
      "name": "Eclipse Public License 1.0",
      "spdx": "EPL-1.0"
    },
    "eupl-1.1": {
      "name": "European Union Public License 1.1",
      "spdx": "EUPL-1.1"
    },
    "agpl-3.0": {
      "name": "GNU Affero General Public License v3.0",
      "spdx": "AGPL-3.0"
    },
    "gpl-2.0": {
      "name": "GNU General Public License v2.0",
      "spdx": "GPL-2.0-only"
    },
    "gpl-3.0": {
      "name": "GNU General Public License v3.0",
      "spdx": "GPL-3.0-only"
    },
    "lgpl-2.1": {
      "name": "GNU Lesser General Public License v2.1",
      "spdx": "LGPL-2.1-only"
    },
    "lgpl-3.0": {
      "name": "GNU Lesser General Public License v3.0",
      "spdx": "LGPL-3.0-only"
    },
    "isc": {
      "name": "ISC",
      "spdx": "ISC"
    },
    "lppl-1.3c": {
      "name": "LaTeX Project Public License v1.3c",
      "spdx": "LPPL-1.3c"
    },
    "ms-pl": {
      "name": "Microsoft Public License",
      "spdx": "MS-PL"
    },
    "mit": {
      "name": "MIT",
      "spdx": "MIT"
    },
    "mpl-2.0": {
      "name": "Mozilla Public License 2.0",
      "spdx": "MPL-2.0"
    },
    "osl-3.0": {
      "name": "Open Software License 3.0",
      "spdx": "OSL-3.0"
    },
    "postgresql": {
      "name": "PostgreSQL License",
      "spdx": "PostgreSQL"
    },
    "ofl-1.1": {
      "name": "SIL Open Font License 1.1",
      "spdx": "OFL-1.1"
    },
    "ncsa": {
      "name": "University of Illinois/NCSA Open Source License",
      "spdx": "NCSA"
    },
    "unlicense": {
      "name": "The Unlicense",
      "spdx": "Unlicense"
    },
    "zlib": {
      "name": "zLib License",
      "spdx": "Zlib"
    }
  }
  ```
</details>

## Academic Free License v3.0

**Key**: `afl-3.0`
**SPDX**: `AFL-3.0`
<kbd><a href="../../wikis/AFL-3.0">Text</a></kbd>

## Apache license 2.0

**Key**: `apache-2.0`
**SPDX**: `Apache-2.0`
<kbd><a href="../../wikis/Apache-2.0">Text</a></kbd>

## Artistic license 2.0

**Key**: `artistic-2.0`
**SPDX**: `Artistic-2.0`
<kbd><a href="../../wikis/Artistic-2.0">Text</a></kbd>

## Boost Software License 1.0

**Key**: `bsl-1.0`
**SPDX**: `BSL-1.0`
<kbd><a href="../../wikis/BSL-1.0">Text</a></kbd>

## BSD 2-clause "Simplified" license

**Key**: `bsd-2-clause`
**SPDX**: `BSD-2-Clause`
<kbd><a href="../../wikis/BSD-2-Clause">Text</a></kbd>

## BSD 3-clause "New" or "Revised" license

**Key**: `bsd-3-clause`
**SPDX**: `BSD-3-Clause`
<kbd><a href="../../wikis/BSD-3-Clause">Text</a></kbd>

## BSD 3-clause Clear license

**Key**: `bsd-3-clause-clear`
**SPDX**: `BSD-3-Clause-Clear`
<kbd><a href="../../wikis/BSD-3-Clause-Clear">Text</a></kbd>

## Creative Commons Zero v1.0 Universal

**Key**: `cc0-1.0`
**SPDX**: `CC0-1.0`
<kbd><a href="../../wikis/CC0-1.0">Text</a></kbd>


<a href="../../wikis/CC0-1.0"><img src="icons/cc0-1.0.svg" alt="Creative Commons Zero v1.0 Universal"></a>
    
## Creative Commons Attribution 4.0

**Key**: `cc-by-4.0`
**SPDX**: `CC-BY-4.0`
<kbd><a href="../../wikis/CC-BY-4.0">Text</a></kbd>


<a href="../../wikis/CC-BY-4.0"><img src="icons/cc-by-4.0.svg" alt="Creative Commons Attribution 4.0"></a>
    
## Creative Commons Attribution Share Alike 4.0

**Key**: `cc-by-sa-4.0`
**SPDX**: `CC-BY-SA-4.0`
<kbd><a href="../../wikis/CC-BY-SA-4.0">Text</a></kbd>


<a href="../../wikis/CC-BY-SA-4.0"><img src="icons/cc-by-sa-4.0.svg" alt="Creative Commons Attribution Share Alike 4.0"></a>
    
## Do What The F*ck You Want To Public License

**Key**: `wtfpl`
**SPDX**: `WTFPL`
<kbd><a href="../../wikis/WTFPL">Text</a></kbd>

## Educational Community License v2.0

**Key**: `ecl-2.0`
**SPDX**: `ECL-2.0`
<kbd><a href="../../wikis/ECL-2.0">Text</a></kbd>

## Eclipse Public License 1.0

**Key**: `epl-1.0`
**SPDX**: `EPL-1.0`
<kbd><a href="../../wikis/EPL-1.0">Text</a></kbd>

## European Union Public License 1.1

**Key**: `eupl-1.1`
**SPDX**: `EUPL-1.1`
<kbd><a href="../../wikis/EUPL-1.1">Text</a></kbd>

## GNU Affero General Public License v3.0

**Key**: `agpl-3.0`
**SPDX**: `AGPL-3.0`
<kbd><a href="../../wikis/AGPL-3.0">Text</a></kbd>


<a href="../../wikis/AGPL-3.0"><img src="icons/agpl-3.0.svg" alt="GNU Affero General Public License v3.0"></a>
    
<details><summary><strong><a name="in-code-assertion">In-Code Assertion</a></strong></summary>

```js
/*!
 * my-new-package: {{ description }}
 *
 * Copyright (C) {{ year }}  {{ legal_name }}
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
```
</details>

## GNU General Public License v2.0

**Key**: `gpl-2.0`
**SPDX**: `GPL-2.0-only`
<kbd><a href="../../wikis/GPL-2.0-only">Text</a></kbd>

## GNU General Public License v3.0

**Key**: `gpl-3.0`
**SPDX**: `GPL-3.0-only`
<kbd><a href="../../wikis/GPL-3.0-only">Text</a></kbd>


<a href="../../wikis/GPL-3.0-only"><img src="icons/gpl-3.0.svg" alt="GNU General Public License v3.0"></a>
    
<details><summary><strong><a name="in-code-assertion">In-Code Assertion</a></strong></summary>

```js
/*!
 * my-new-package: {{ description }}
 *
 * Copyright (C) {{ year }}  {{ legal_name }}
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
```
</details>

## GNU Lesser General Public License v2.1

**Key**: `lgpl-2.1`
**SPDX**: `LGPL-2.1-only`
<kbd><a href="../../wikis/LGPL-2.1-only">Text</a></kbd>

## GNU Lesser General Public License v3.0

**Key**: `lgpl-3.0`
**SPDX**: `LGPL-3.0-only`
<kbd><a href="../../wikis/LGPL-3.0-only">Text</a></kbd>


<a href="../../wikis/LGPL-3.0-only"><img src="icons/lgpl-3.0.svg" alt="GNU Lesser General Public License v3.0"></a>
    
<details><summary><strong><a name="in-code-assertion">In-Code Assertion</a></strong></summary>

```js
/*!
 * my-new-package: {{ description }}
 *
 * Copyright (C) {{ year }}  {{ legal_name }}
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
```
</details>

## ISC

**Key**: `isc`
**SPDX**: `ISC`
<kbd><a href="../../wikis/ISC">Text</a></kbd>

## LaTeX Project Public License v1.3c

**Key**: `lppl-1.3c`
**SPDX**: `LPPL-1.3c`
<kbd><a href="../../wikis/LPPL-1.3c">Text</a></kbd>

## Microsoft Public License

**Key**: `ms-pl`
**SPDX**: `MS-PL`
<kbd><a href="../../wikis/MS-PL">Text</a></kbd>

## MIT

**Key**: `mit`
**SPDX**: `MIT`
<kbd><a href="../../wikis/MIT">Text</a></kbd>

<details><summary><strong><a name="in-code-assertion">In-Code Assertion</a></strong></summary>

```js
/*!
 * my-new-package
 * {{ repo.html_url }}
 * Copyright(c) {{ year }} {{ legal_name }}
 * MIT Licensed
 */
```
</details>

## Mozilla Public License 2.0

**Key**: `mpl-2.0`
**SPDX**: `MPL-2.0`
<kbd><a href="../../wikis/MPL-2.0">Text</a></kbd>

## Open Software License 3.0

**Key**: `osl-3.0`
**SPDX**: `OSL-3.0`
<kbd><a href="../../wikis/OSL-3.0">Text</a></kbd>

## PostgreSQL License

**Key**: `postgresql`
**SPDX**: `PostgreSQL`
<kbd><a href="../../wikis/PostgreSQL">Text</a></kbd>

## SIL Open Font License 1.1

**Key**: `ofl-1.1`
**SPDX**: `OFL-1.1`
<kbd><a href="../../wikis/OFL-1.1">Text</a></kbd>

## University of Illinois/NCSA Open Source License

**Key**: `ncsa`
**SPDX**: `NCSA`
<kbd><a href="../../wikis/NCSA">Text</a></kbd>

## The Unlicense

**Key**: `unlicense`
**SPDX**: `Unlicense`
<kbd><a href="../../wikis/Unlicense">Text</a></kbd>

## zLib License

**Key**: `zlib`
**SPDX**: `Zlib`
<kbd><a href="../../wikis/Zlib">Text</a></kbd>
