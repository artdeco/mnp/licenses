import list from '../list'
import { readFileSync, existsSync, writeFileSync } from 'fs'

export const licenses = () => {
  return Object.entries(list).map(([key, { name, spdx }]) => {
    const textPath = `assertions/${key}.js`
    const iconPath = `icons/${key}.svg`
    const e = existsSync(textPath)
    const i = existsSync(iconPath)
    const text = e ? readFileSync(textPath) : ''
    const icon = i ? iconPath : ''
    let s = `## ${name}

**Key**: \`${key}\`
**SPDX**: \`${spdx}\`
<kbd><a href="../../wikis/${spdx}">Text</a></kbd>
`
    if (icon) s += `

<a href="../../wikis/${spdx}"><img src="${icon}" alt="${name}"></a>
    `
    if (text) {
      s += `
<details><summary><strong>[In-Code Assertion](t)</strong></summary>

\`\`\`js
${text}
\`\`\`
</details>
`
    }
    writeFileSync(`wiki/${spdx}.md`, `
\`\`\`
${readFileSync(`licenses/${key}.txt`)}
\`\`\`
    `)
    return s
  })
}