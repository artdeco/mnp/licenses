## 26 March 2020

### [1.1.0](https://gitlab.com/artdeco/mnp/licenses/compare/v1.0.0...v1.1.0)

- [doc] Better documentation and icons.
- [feature] Add in-code assertions for MIT, [A/L]GPL-3.0.

## 1 December 2019

### 1.0.0

- Add a list of 30 licenses.